use std::net::{TcpStream,TcpListener};
use std::error;
use std::time::Duration;
use std::thread;
use std::io;
use std::io::Read;
use std::io::Write;



fn handle(mut stream: TcpStream) -> Result<(),Box<error::Error>>   {
    // try reading
    //
    let mut buf = [0;256];
    stream.read(&mut buf);
    println!("--{}--",String::from_utf8(buf.to_vec()).expect("string convert fail"));
    Ok(())
}

fn main() -> Result<(),Box<error::Error>> {
    let ser = TcpListener::bind("127.0.0.1:8888");
    for con in ser?.incoming() {
        handle(con?);
    }
    Ok(())
}

//fn other() -> Result<(),Box<error::Error>> {
//    let mut stream = TcpStream::connect("127.0.0.1:8080")?;
//    stream.write("end".as_bytes());
//    loop {
//        println!("looping");
//        let num_read = stream.read(&mut buf)?;
//        println!("{} read",num_read);
//        println!("{}",String::from_utf8(buf.to_vec())?);
//        buf = [0;256];
//    }
//    Ok(())
//}
