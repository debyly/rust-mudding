# Explanation of the file structure

## Telly

this used to be a monolithic version of what became programs one two and three in the scrap directory.

it used to be responsible for communicating with the telnet world server. It also reacted to scripted text input events like "you are almost dead" would probably send the server a command like "flee!". 

!! note that this is now recreated 

## Scrap

I apologize for the naming structure, this was created while I was trying to figure out how to have three separate sockets live, and was testing things out hence the name scrap.

### One

this is the new home of the telnet program. it communicates to separate processes for the user input and the scripted behavior over sockets.

I don't think there's too much I would like help thinking about architecture wise in this file.


### Two

This program takes input from the user and sends it over the socket to the One process.


### Three 

This is where things get pretty messy. I don't have a good sense about how to increase code reuse for this file. It's goal is to have a number of different categories of behavior as to how to respond to specific input from the game.

The maze module is additional code to help explore a space of certain dimension without hitting too many places more than once.




