use rand::random;
use std::io::{Read, Write};
use std::net::{TcpListener, TcpStream};
use std::time::{Duration, SystemTime};
use std::thread;
use std::env;

mod maze;
use maze::Pather;

fn log(s: String) {
    // send it to /tmp/mudlog
    let mut stream = TcpStream::connect("127.0.0.1:8888").expect("failed connection for log");
    stream.write(s.as_bytes());
}

#[derive(Debug)]
enum PlayerState {
    Fighting,
    Standing,
    Busy,
    Resting,
}

enum CommandState {
    First,
    Second,
}

#[derive(Debug)]
struct WorldRep {
    pstate: PlayerState,
    permissions:Vec<String>,
}



fn inbetween(start: &str, end: &str, content: &str) -> String {
    let f_ind = content.find(start).expect("no start found") + start.len();
    let s_ind = content[f_ind..].find(end).expect("didn't find an end");
    return content[f_ind..f_ind + s_ind].into();
}

struct Player {
    name: String,
    pw: String,
    commands: Vec<String>,
    favorite_prey: Vec<String>,
}

impl Player {
    fn new(name: &str, pw: &str, commands: Vec<&str>, favorite_prey: Vec<&str>) -> Player {
        Player {
            name: name.into(),
            pw: pw.into(),
            commands: commands
                .into_iter()
                .map(|x| x.into())
                .collect::<Vec<String>>(),
            favorite_prey: favorite_prey
                .into_iter()
                .map(|x| x.into())
                .collect::<Vec<String>>(),
        }
    }
}
trait Event {
    fn respond(&mut self, input: Option<&String>, world: &mut WorldRep) -> Option<String>;
}

struct Chop;
impl Event for Chop {
    fn respond(&mut self, input: Option<&String>, world: &mut WorldRep) -> Option<String> {
        match input {
            Some(s) => {
                //println!("checking chop");
                if s.contains("commandchop") {
                    //println!("found chop");
                    return Some("chop".into());
                }
            }
            _ => {}
        }
        None
    }
}

// send enter response every now and then
struct HeartBeat {
    timer: SystemTime,
    on: bool,
}

impl Event for HeartBeat {
    fn respond(&mut self, input: Option<&String>, world: &mut WorldRep) -> Option<String> {
        match input {
            Some(s) => {
                if s.contains("hbeat-on") {
                    self.on = true;
                }
                if s.contains("hbeat-off") {
                    self.on = false;
                }
            }
            _ => {}
        }
        if self.on {
            if self.timer.elapsed().unwrap() > Duration::new(3, 0) {
                self.timer = SystemTime::now();
                return Some("l\n".to_string());
            }
        }
        return None;
    }
}

enum RestState {
    Resting,
    Standing,
}

struct AutoKill {
    on: bool,
    fighting: bool,
    targets: Vec<String>,
    command_i: usize,
    timer: SystemTime,
    state: RestState,
    target: String,
    kill_commands: Vec<String>,
}

impl Event for AutoKill {
    fn respond(&mut self, input: Option<&String>, world: &mut WorldRep) -> Option<String> {
        match input {
            // check if we are turning on or off the random walk
            Some(s) => {
                if s.contains("killcommand") {
                    //println!("entering kill command");
                    self.on = !self.on; // toggle the on nature of walk
                                        //println!("on is {:?}", self.on);
                    match s.split("/").nth(1) {
                        Some(t) => self.kill_commands.push(t.to_string().clone()),
                        _ => println!("no second argument given to autokill"),
                    }
                }
                if s.contains("autokill") {
                    //println!("changing AutoKill");
                    let prey = inbetween("autokill-", "-", s);
                    //println!("setting on kill for -{}-", prey);
                    self.targets.push(prey.clone());
                    //println!("self.targets {:?}",self.targets);
                    log(format!("{:?}", self.targets));
                }
                // assume this is a room description
                if s.contains("not here")
                    || s.contains("cancel")
                    || s.contains("anyone")
                    || s.contains("confusion")
                    || s.contains("NOBITS")
                {
                    self.fighting = false;
                    world.pstate = PlayerState::Standing;
                    return None;
                }
                if s.contains("R.I.P") {
                    self.fighting = false;
                    world.pstate = PlayerState::Standing;
                    return Some("take all corpse".to_string());
                }
                if s.contains("kill") {
                    self.fighting = true;
                    world.pstate = PlayerState::Fighting;
                }
                // responses
                if self.timer.elapsed().unwrap() > Duration::new(4, 0) {
                    self.timer = SystemTime::now();
                    log(format!("checking fighting {}", self.fighting));
                    if !self.fighting {
                        // if no exits don't search for targets
                        match s.find("Exits") {
                            Some(ind) => {
                                let tail = s[ind..].to_string();
                                log(format!("tail{}tail", tail));
                                for t in self.targets.iter() {
                                    //println!("scanning for target --{}-- in --{}--",t,s);
                                    //take part of text after exits

                                    if tail.to_lowercase().contains(&t.to_lowercase()) {
                                        self.fighting = true;
                                        self.target = t.to_string();
                                        world.pstate = PlayerState::Fighting;
                                        log(format!("targeting{} ", t));
                                        return Some(format!("kill {}", t));
                                    }
                                }
                            }
                            _ => {}
                        }
                    }
                    if self.fighting {
                        //println!("tring kick");
                        if self.kill_commands.len() > 0 {
                            self.command_i = (self.command_i + 1) % self.kill_commands.len();
                            log(format!("triggerinreturning kick?{}", self.command_i));
                            return Some(self.kill_commands[self.command_i].clone());
                        }
                    }
                }
            }
            _ => {}
        }
        // if wew reach here return None
        None
    }
}

enum HungerResponse {
    Eating,
    Drinking,
}

struct Puppet;
impl Event for Puppet {
    fn respond(&mut self, input: Option<&String>, world: &mut WorldRep) -> Option<String> {
        match input {
            Some(s) => {
                if s.contains("ppt-") {
                    // this is a command to be echoed by the bot player
                    let command = inbetween("ppt-", "-", &s);
                    return Some(command);
                }
            }
            _ => {}
        }
        return None;
    }
}

struct Repeat;

impl Event for Repeat {
    fn respond(&mut self, input: Option<&String>, world: &mut WorldRep) -> Option<String> {
        match input {
            Some(s) => {
                if s.contains("rep-") {
                    let command = inbetween("rep-", "x", &s);
                    let times = inbetween("x", "-", &s);
                    println!(
                        "repeating {}",
                        command.repeat(times.parse::<usize>().unwrap()).to_string()
                    );
                    return Some(
                        format!("{}\n", command)
                            .repeat(times.parse::<usize>().unwrap())
                            .to_string(),
                    );
                }
            }
            _ => {}
        }
        None
    }
}

struct Grouping {}

impl Event for Grouping {
    fn respond(&mut self, input: Option<&String>, world: &mut WorldRep) -> Option<String> {
        match input {
            Some(s) => {
                if s.contains("says, 'follow") {
                    let speaker = s.split_whitespace().nth(0).unwrap();
                    return Some(format!("follow {}", speaker));
                }
                //if s.contains("group") {
                //   let speaker =  s.split_whitespace().nth(0).unwrap() ;
                //        return Some(format!("group {}",speaker))
                //}
            }
            _ => {}
        }
        return None;
    }
}

struct EatDrink {
    state: HungerResponse,
}

impl Event for EatDrink {
    fn respond(&mut self, input: Option<&String>, world: &mut WorldRep) -> Option<String> {
        match input {
            Some(s) => {
                if s.contains("hungry") || s.contains("STARVING") {
                    //self.state = HungerResponse::Eating;
                    //return Some("eat loaf".into());
                }
                if s.contains("thirst") {
                    //self.state = HungerResponse::Drinking;
                    //////println!("drinking bottle");
                    //return Some("drink bottle".into());
                }
                // second level
                if s.contains("You do not have that item.") | s.contains("Drink what?") {
                    match self.state {
                        HungerResponse::Eating => {
                            //println!("getting rabbit meat from bag");
                            return Some("get meat bag".into());
                        }
                        HungerResponse::Drinking => {
                            //println!("getting a dragonskin from bag");
                            return Some("get dragonskin bag".into());
                        }
                    }
                }
            }
            _ => {}
        }
        return None;
    }
}

struct CommandPosition {}

impl Event for CommandPosition {
    fn respond(&mut self, input: Option<&String>, world: &mut WorldRep) -> Option<String> {
        match input {
            Some(s) => {
                if s.contains("commandstand") {
                    return Some("stand".into());
                }
                if s.contains("commandrest") {
                    return Some("rest".into());
                }
            }
            _ => {}
        }
        return None;
    }
}

// similar is the maze walker which ensures that all points are hit in an area if possible

struct RandomWalk {
    on: bool,
    return_path: Vec<String>,
    timer: SystemTime,
}

impl RandomWalk {
    fn add_in(&mut self, dir: String) {
        if self.return_path.len() == 0 {
            self.return_path.push(dir)
        } else if dir == "E" && self.return_path.last().map(|e| e != "W").unwrap() {
            self.return_path.push(dir);
        } else if dir == "W" && self.return_path.last().map(|e| e != "E").unwrap() {
            self.return_path.push(dir);
        } else if dir == "N" && self.return_path.last().map(|e| e != "S").unwrap() {
            self.return_path.push(dir);
        } else if dir == "S" && self.return_path.last().map(|e| e != "N").unwrap() {
            self.return_path.push(dir);
        }
    }
    fn go_back(&mut self) -> Option<String> {
        self.return_path.pop()
    }
}

impl Event for RandomWalk {
    fn respond(&mut self, input: Option<&String>, world: &mut WorldRep) -> Option<String> {
        match input {
            // check if we are turning on or off the random walk
            Some(s) => {
                if s.contains("makeroute") {
                    let order = inbetween("makeroute-", "-", &s);
                    self.return_path = order
                        .chars()
                        .map(|e| e.to_string())
                        .collect::<Vec<String>>();
                    self.return_path.reverse();
                }
                if s.contains("go-back") {
                    return self.go_back();
                }
                if s.contains("randw-on") {
                    //println!("changing random walking");
                    self.on = true; // toggle the on nature of walk
                                    //println!("on is {:?}", self.on);
                }
                if s.contains("randw-off") {
                    //println!("turning off randw");
                    self.on = false;
                }
                if self.timer.elapsed().unwrap() > Duration::new(5, 0) {
                    self.timer = SystemTime::now();
                    if self.on {
                        let ind = random::<usize>() % 4;
                        let dir = "NESW".chars().nth(ind).unwrap().to_string();
                        self.add_in(dir.clone());

                        return Some(dir);
                    }
                    if self.return_path.len() > 0 {
                        return self.go_back();
                    }
                }
            }
            _ => {}
        }
        // if wew reach here return None
        None
    }
}

struct Mazer {
    exits: Option<Vec<String>>,
    pth: maze::Pather,
    on:MazeState,
    state: CommandState,
    timer: SystemTime,
}

impl Mazer {
    fn new() -> Mazer {
        let pather = Pather::setup(7,7);
        Mazer {
            pth: pather,
            exits: None,
            on: MazeState::Off,
            state: CommandState::First,
            timer: SystemTime::now(),
        }
    }
}

enum MazeState {
    On,
    Off,
    Backtracking
}

impl Event for Mazer {
    fn respond(&mut self, input: Option<&String>, world: &mut WorldRep) -> Option<String> {
        match world.pstate {
            PlayerState::Standing=> {
            }
            _ => {
                log(format!("can't maze"));
                return None
            }
        }
        match input {
            Some(s) => {
                if s.contains("+----") {
                    self.exits = Some(vec![]);
                    self.state = CommandState::Second;
                    log(format!("trying to maze"));
                }
                if s.contains("mazer-on") {
                    self.on = MazeState::On;
                }
                if s.contains("mazer-off") {
                    self.on = MazeState::Off;
                    // reset the path
                    *self = Mazer::new();
                }
                if s.contains("backtrack") {
                    self.state = CommandState::Second;
                    self.on = MazeState::Backtracking;

                }
            }
            _ => {}
        }
        match self.state {
            CommandState::Second => {
                match self.on {
                   MazeState::On =>  {
                        if self.timer.elapsed().unwrap() > Duration::new(5, 0) {
                            // go to one of the existing possible exits
                            // change the world state to second
                            // so now call step knowing what we know about the possible exits
                            self.timer = SystemTime::now();
                            // go back to first
                            self.state = CommandState::First;
                            return self.exits.clone().map(|ex| return self.pth.step(ex));
                        }
                    },
                    MazeState::Backtracking => {
                        let dir = self.pth.pth.pop();
                        return dir
                    },
                    _=> {}
                }
            }
            _ => {}
        }
        None
    }
}
fn state_eval(s:&str,world:&mut WorldRep) -> Option<String> {
    match world.pstate {
        PlayerState::Standing => {
            if s.contains("|wounded")|| s.contains("/exhausted") || s.contains("|hurt") || s.contains("|mauled") || s.contains("|injured") || s.contains("|crippled") {
                world.pstate = PlayerState::Resting;
                return Some(format!("rest \n"))
            }
            return None
        },
        PlayerState::Fighting => {
            //if s.contains("|wounded")  || s.contains("|injured") {
            //    return Some(format!("firstaid\n"))
            //}
            if s.contains("|crippled") || s.contains("|mauled") {
                return Some(format!("flee\n"))
            }
            return None
        },
        PlayerState::Resting => {
            if s.contains("|healthy/rested") {
                world.pstate = PlayerState::Standing;
                return Some(format!("stand \n"))
            }
            return None
        },
        _ => return None
    }
}


fn abort(s:&str,world:&mut WorldRep) -> Option<String> {
    match world.pstate {
        PlayerState::Standing => {
            return None
        },
        _ => {
            if s.contains("-stop-")  {
                world.pstate = PlayerState::Standing;
                return Some("stop".to_string());
            }
            None
        }
    }
}

fn searchkill(s: &str, world: &mut WorldRep) -> Option<String> {
    // skip trying to kill things while already fighting or sleeping
    match world.pstate {
        PlayerState::Fighting | PlayerState::Resting => {
            if s.contains("R.I.P.") || s.contains("don't seem to be here") {
                world.pstate = PlayerState::Standing;
                return Some(format!("get corpse\n"))
            }
            return None;
        }
        _ => {}
    }
    let targets: Vec<_> = "sheep chicken rooster rabbit peccary squirrel opossum pig cat quail"
        .split(" ")
        .map(|e| e)
        .collect();
    println!("checking targets");
    for t in targets {
        println!("comparing {}", t);
        if s.contains(&format!(" {} ",t)) {
            println!("found {}", t);
            world.pstate = PlayerState::Fighting;
            return Some(format!("kill {}\n", t));
        }
    }
    None
}
fn heartbeat() {
    let main_port = env::args().nth(1).expect("missing first arg");
    let mut ms = TcpStream::connect(format!("127.0.0.1:{}",main_port)).expect("main stream fail");
    let mut now = SystemTime::now();
    loop {
        if now.elapsed().expect("time fail") > Duration::new(6,0) {
            now = SystemTime::now();
            ms.write("l\n".as_bytes());
        } else {
        ms.write("\n\n".as_bytes());
        }
        thread::sleep(Duration::new(2,0));
    }
}



fn harvest(s:&str,world:&mut WorldRep) -> Option<String> {
    if s.contains(", harvest") {
        return match world.pstate{
            PlayerState::Standing => {
                world.pstate = PlayerState::Busy;
                Some("harvest \n".to_string())
            },
            _=> None
        }
    }
    if s.contains("Seeded Field") {
        return match world.pstate {
            PlayerState::Busy => {
                world.pstate = PlayerState::Standing;
                None
            },
            _ => None
        }
    }
    if s.contains("Open Plains") {
        return match world.pstate {
            PlayerState::Busy => {
                // finished but didn't register correctly
                world.pstate = PlayerState::Standing;
                None
            },
            PlayerState::Standing => {
                world.pstate = PlayerState::Busy;
                return Some("plant barley\n".to_string())
            },
            _ => None
        }
    }
    if s.contains("finish harvesting") {
        return match world.pstate {
           PlayerState::Busy=> {
               world.pstate = PlayerState::Standing;
               None
           },
           _ => None
        }
    }
    return None
}

fn chopping(s:&str,world:&mut WorldRep) -> Option<String> {
    if s.contains("Forest") {
        return match world.pstate {
            PlayerState::Standing => {
                world.pstate = PlayerState::Busy;
                Some("chop \n".to_string())
            },
            _=> None
        }
    }
    if s.contains("Open Plains") {
        // they've harvested all they can at this point
        match world.pstate {
            PlayerState::Busy => {
                world.pstate = PlayerState::Standing;
            },
            _=> {}
        } 
        return None

    }
    if s.contains("crack") {
        //return match world.pstate {
        //    PlayerState::Busy => {
        //    world.pstate = PlayerState::Standing;
        //    Some("stop \n".to_string())
        //    }
        //    _ => None
        //}
    }
    None
}


fn set_perm(s:&str,world:&mut WorldRep) {
    if s.contains("set-chop") {
        world.permissions.push("chop".to_string());
    }
    if s.contains("set-harvest") {
        world.permissions.push("harvest".to_string());
    }
    if s.contains("set-hunt") {
        world.permissions.push("hunt".to_string());
    }
}

// get input from telnet then write back to existing socket server so there's no wait for the
// telnet thread and then the mpsc is the only communicator
fn main() {
    let main_port = env::args().nth(1).expect("missing first arg");
    let port:u16 = 45000 + random::<u8>() as u16;
    let mut listener = TcpListener::bind(format!("127.0.0.1:{}",port)).expect("fail binding");
    let mut main_stream = TcpStream::connect(format!("127.0.0.1:{}",main_port)).expect("main stream fail");
    main_stream.write(format!("-identify-,{},",port).as_bytes());
    let mut buf = [0; 256 * 8];
    let mut mazer = Mazer::new();
    let mut busy_check = 0;
    let mut world = WorldRep {
        pstate: PlayerState::Standing,
        permissions:vec![],
    };
    // heartbeat
    thread::spawn(heartbeat);
    for s in listener.incoming() {
        // send status log to listener
        log(format!("status is {:?}",world));
        let mut s = s.expect("broken socket");
        // initiate the event tracking stuff
        s.read(&mut buf);
        let s = String::from_utf8(buf.to_vec()).expect("string fail");
        buf = [0; 256 * 8];
        println!("s is {}", s);
        log(format!("state {:?}",world.pstate));
        set_perm(&s,&mut world);
        state_eval(&s,&mut world).map(|s| {
            println!("changing state");
            main_stream.write(s.as_bytes());
        });
        // check for too long busy
        match world.pstate {
            PlayerState::Busy => {
                busy_check +=1;
                println!("busy calc {}",busy_check);
                if busy_check > 50 {
                    println!("busy stall");
                    main_stream.write("say -stop-\n".as_bytes());
                    world.pstate = PlayerState::Standing;
                    busy_check = 0;
                }
            },
            _ => {
                busy_check = 0;
            }
        }
        abort(&s,&mut world).map(|s| {
            main_stream.write(s.as_bytes());
        });
        for perm in world.permissions.clone(){
            if perm.contains("harvest") {
                harvest(&s,&mut world).map(|s| {
                    println!("harvesting");
                    main_stream.write(s.as_bytes());
                });
            }
            if perm.contains("hunt") {
                searchkill(&s,&mut world).map(|s| {
                    println!("trying to kill {}", s);
                    main_stream.write(s.as_bytes())
                });
            }
            if perm.contains("chop") {
                chopping(&s,&mut world).map(|s|{
                    main_stream.write(s.as_bytes())
                });
            }
        }
        mazer.respond(Some(&s.to_string()), &mut world).map(|s| {
            println!("mazer sayz {}", s);
            main_stream.write(format!("{}\n", s).as_bytes());
        });
    }
    // connect to the main listener
}

