use rand;
use std::env;
use std::io::{Read, Write};
use std::net::{TcpListener, TcpStream};
use std::sync::mpsc::channel;
use std::thread;
use std::time::{Duration, SystemTime};
use telnet::{Telnet, TelnetEvent};

fn handle(mut connection: TcpStream, tx: std::sync::mpsc::Sender<String>) {
    let mut buf = [0; 256];
    loop {
        let res = connection.read(&mut buf);
        match res {
            Ok(e) => {
                if e == 0 {
                    break;
                }
            }
            _ => {}
        }
        let content = String::from_utf8(buf.to_vec()).expect("string fail");
        tx.send(content);
        buf = [0; 256];
    }
}

fn log(s: String) {
    // send it to /tmp/mudlog
    let mut stream = TcpStream::connect("127.0.0.1:8888").expect("failed connection for log");
    stream.write(s.as_bytes());
}

fn telly(rx: std::sync::mpsc::Receiver<String>) {
    let mut telnet = Telnet::connect(("127.0.0.1", 4000), 256 * 8).expect("no connection");
    let mut e_port = vec![];
    loop {
        let event = telnet
            .read_timeout(Duration::new(1, 0))
            .expect("reading error");
        //read telnet data

        match event {
            TelnetEvent::Data(buffer) => {
                let content = String::from_utf8(buffer.to_vec()).map(|s| {
                    // send it back to the websocket
                    // this part is easy with the sender mesage
                    println!("{}", s.trim());
                    e_port.last().map(|p| {
                    match TcpStream::connect(format!("127.0.0.1:{}",p)) {
                        Ok(mut event_socket) => {
                            event_socket.write(s.trim().as_bytes());
                        }
                        Err(e) => {
                            println!("error {:?} pval", e);
                        }
                    }
                    });
                });
            }
            TelnetEvent::TimedOut
            | TelnetEvent::Negotiation(_, _)
            | TelnetEvent::Subnegotiation(_, _)
            | TelnetEvent::UnknownIAC(_) => {}

            e => {
                println!("ending telnet loop {:?}", e);
                break;
            }
        }
        //
        //check for input from the socket threads
        match rx.try_recv() {
            Ok(content) => {
                if content.contains("-identify-") {
                    e_port.push(content.split(",").nth(1).expect("no comma").to_string());
                    println!("port provided -{:?}-", e_port);
                // this isn't a message to send to the telnet
                } else {
                    log(content.clone());
                    telnet.write(content.as_bytes()).expect("telnet write fail");
                }
            }
            _ => {}
        }
        thread::sleep(Duration::new(0, 6));
    }
}

fn main() {
    // start off by talking to the listening server with register token
    //
    let port:u16 = rand::random::<u8>() as u16 + 45000; 
    println!("PORT {} {}", port, format!("127.0.0.1:{}", port));
    let server = TcpListener::bind(format!("127.0.0.1:{}", port)).expect("server no start");
    let mut buf = [0; 256];
    // could setup the telnet in a thread on its own with mpsc being given the messages?
    //create the telnet thread, but also supply the mpsc receiver, give producers to the event and
    //input thread
    let (tx, rx) = channel();
    thread::spawn(move || {
        telly(rx);
    });
    for inc in server.incoming() {
        let mut inc = inc.expect("incoming fail");
        println!("spawning");
        let thread_trans = tx.clone();
        thread::spawn(move || {
            handle(inc, thread_trans);
        });
        // keep alive as demonstration
    }
}
