use common;
pub struct Pather {
    pub pth: Vec<String>,
    A: Area,
    wlkr: Walker,
}

impl Pather {
    pub fn setup(w: usize, h: usize) -> Pather {
        let mut walker = Walker {
            backtrack: vec![],
            x: 5,
            y: 5,
        };
        let mut A = Area::new(w, h);
        let mut pth = Pather {
            pth: vec![],
            A,
            wlkr: walker,
        };
        pth
    }
    pub fn step(&mut self, exits: Vec<String>) -> String {
        let res = self.wlkr.step(&mut self.A, exits);

        self.A.show_map([self.wlkr.x, self.wlkr.y]);
        println!("{:?} is res", res);
        let out = match res {
            [-1, 0] => "w".into(),
            [1, 0] => "e".into(),
            [0, 1] => "s".into(),
            [0, -1] => "n".into(),
            _ => "x".into(),
        };
        println!("out is {}", out);
        out
    }
}

struct Walker {
    backtrack: Vec<[i32; 2]>,
    x: usize,
    y: usize,
}

impl Walker {
    fn step(&mut self, A: &mut Area, exits: Vec<String>) -> [i32; 2] {
        // watch out for walls and corners
        //could just be careless and use nths
        let mut array_options = vec![];
        let mut walker_options: Vec<[i32; 2]> = vec![];
        let tempx = self.x as i32;
        let tempy = self.y as i32;
        for e in exits.into_iter() {
            println!("e is {}", &e[..]);
            let pair_ind = match &e[..] {
                "n" => [0, -1],
                "s" => [0, 1],
                "e" => [1, 0],
                "w" => [-1, 0],
                _ => continue, // hopefully this doesn't come up
            };
            println!("pair ind is {:?}", pair_ind);
            // mark off these places
            A.get_cell_index(pair_ind[0] + tempx, pair_ind[1] + tempy)
                .map(|index| {
                    A.array[index] = '!';
                });
        }
        //eliminate the indices that the array doesn't actually have
        A.get_cell_index(tempx + 1, tempy).map(|index| {
            array_options.push(index);
            walker_options.push([1, 0]);
        });
        A.get_cell_index(tempx - 1, tempy).map(|index| {
            array_options.push(index);
            walker_options.push([-1, 0]);
        });
        A.get_cell_index(tempx, tempy + 1).map(|index| {
            array_options.push(index);
            walker_options.push([0, 1]);
        });
        A.get_cell_index(tempx, tempy - 1).map(|index| {
            array_options.push(index);
            walker_options.push([0, -1]);
        });
        // pick one of the directions
        let rand = (common::rand::num() * (walker_options.len() as f64)).floor() as usize;
        let dir = if array_options.len() == 0 {
            self.backtrack.pop().unwrap()
        } else {
            let ind = array_options[rand];
            self.backtrack
                .push([-1 * walker_options[rand][0], -1 * walker_options[rand][1]]);
            A.array[ind] = 'x';
            walker_options[rand]
        };
        self.x = (self.x as i32 + dir[0]) as usize;
        self.y = (self.y as i32 + dir[1]) as usize;
        dir
        // if len of array options ==0 attempt backtracking
        // use option to update walker and self
    }
}

struct Area {
    width: usize,
    height: usize,
    array: Vec<char>,
}

impl Area {
    fn show_map(&self, target: [usize; 2]) {
        for i in 0..self.height {
            for j in 0..self.width {
                if i == target[1] && j == target[0] {
                    print!("-O-");
                } else {
                    print!("-{}-", self.array[i * self.width + j]);
                }
            }
            println!("");
        }
    }
    // do this so that out of bounds or used spaces don't turn up
    fn get_cell_index(&self, x: i32, y: i32) -> Option<usize> {
        if x < 0 && y < 0 {
            return None;
        }
        let x = x as usize;
        let y = y as usize;
        if x < self.width && y < self.height {
            if self.array[y * self.width + x] == '_' {
                return Some(y * self.width + x);
            }
            return None;
        } else {
            return None;
        }
    }
    pub fn complete(&self) -> bool {
        for i in self.array.iter() {
            if *i == '_' {
                return false;
            }
        }
        return true;
    }
    fn new(width: usize, height: usize) -> Area {
        Area {
            width,
            height,
            array: vec!['_'; width * height],
        }
    }
    fn pout(&self) {}
}

fn main() {}
