use common;
use rand::prelude::*;
use std::fs;
use std::io::Read;
use std::io::Write;
use std::mem;
use std::net::TcpStream;
use std::sync::mpsc;
use std::sync::Arc;
use std::thread;
use std::time::{Duration, SystemTime};
use telnet::{Telnet, TelnetEvent};
use ws;
use ws::{listen, CloseCode, Handler, Handshake, Message, Result, Sender};

mod maze;
use maze::Pather;

fn log(s: String) {
    // send it to /tmp/mudlog
    let mut stream = TcpStream::connect("127.0.0.1:8888").expect("failed connection for log");
    stream.write(s.as_bytes());
}

enum PlayerState {
    Fighting,
    Standing,
    Resting,
}

enum CommandState {
    First,
    Second,
}

struct WorldRep {
    pstate: PlayerState,
}

fn inbetween(start: &str, end: &str, content: &str) -> String {
    let f_ind = content.find(start).expect("no start found") + start.len();
    let s_ind = content[f_ind..].find(end).expect("didn't find an end");
    return content[f_ind..f_ind + s_ind].into();
}

struct Player {
    name: String,
    pw: String,
    commands: Vec<String>,
    favorite_prey: Vec<String>,
}

impl Player {
    fn new(name: &str, pw: &str, commands: Vec<&str>, favorite_prey: Vec<&str>) -> Player {
        Player {
            name: name.into(),
            pw: pw.into(),
            commands: commands
                .into_iter()
                .map(|x| x.into())
                .collect::<Vec<String>>(),
            favorite_prey: favorite_prey
                .into_iter()
                .map(|x| x.into())
                .collect::<Vec<String>>(),
        }
    }
}

struct MultiPlay {
    players: Vec<Player>,
}

fn setup_once(tnet: &mut telnet::Telnet, command: String) {
    thread::sleep(Duration::new(0, 5));
    tnet.write(format!("{}\n", command).as_bytes()).unwrap();
}

fn create_telnet_instance(p: Player, last: bool) {
    let name = p.name;
    let pw = p.pw;
    let commands = p.commands;
    let mut end_loop = false;
    let mut listeners = Listeners::setup_listeners();
    let mut telnet = Telnet::connect(("127.0.0.1", 4000), 256 * 8).expect("no connection");

    // sign in before looping
    // only has to happen once
    thread::sleep(Duration::new(2, 0));
    setup_once(&mut telnet, name.clone());
    setup_once(&mut telnet, pw.clone());
    //setup_once(&mut telnet, "skip".into());

    let mut socket = TcpStream::connect("127.0.0.1:8080").expect("no stream connection");
    if last {
        socket.write("end\n".as_bytes());
    } else {
        socket.write("hi\n".as_bytes());
    }
    socket.set_nonblocking(true);
    let mut buf = [0; 256];
    // wire up autokill and wander
    for c in commands {
        // has to be said to get it into the history to operate on
        setup_once(&mut telnet, format!("say killcommand /{}/", c.clone()));
    }
    for prey in p.favorite_prey {
        setup_once(&mut telnet, format!("say autokill-{}-", prey.clone()));
    }
    //setup_once(&mut telnet,"say randw-on".into());

    let mut existing_text = vec![];
    let mut world = WorldRep {
        pstate: PlayerState::Fighting,
    };
    loop {
        if end_loop {
            break;
        }
        let event = telnet
            .read_timeout(Duration::new(1, 0))
            .expect("reading error");
        match event {
            TelnetEvent::Data(buffer) => {
                let content = String::from_utf8(buffer.to_vec()).map(|s| {
                    existing_text.push(s.clone().trim().to_string());
                    // send it back to the websocket
                    // this part is easy with the sender mesage
                    println!("{}", s.trim());
                });
            }
            TelnetEvent::TimedOut
            | TelnetEvent::Negotiation(_, _)
            | TelnetEvent::Subnegotiation(_, _)
            | TelnetEvent::UnknownIAC(_) => {}

            e => {
                println!("ending telnet loop {:?}", e);
                break;
            }
        }
        // poll for writeout of updates, make into function perhaps?
        // provide the last message provided by the mud
        let mut ev_buf = [0; 256];
        let mut resp_content = None;
        let mut evs = TcpStream::connect("127.0.0.1:8889").expect("no evs");
        evs.set_nonblocking(true);
        evs.write(existing_text[existing_text.len() - 1].as_bytes());
        evs.read(&mut ev_buf).ok().map(|x| {
            if x > 0 {
                let s = String::from_utf8(ev_buf.to_vec()).expect("ev_string fail");
                log(format!("string back from ev is {}",s));
                resp_content = Some(s);
            }
        });
        // trim down the messages holder
        if existing_text.len() > 10 {
            existing_text = existing_text[5..].to_vec();
        }
        // send text along to the telnet session
        match resp_content {
            Some(s) => {
                telnet.write(s.as_bytes()).unwrap();
            }
            _ => {}
        }
        // check for text from socket
        //
        let number = socket.read(&mut buf);
        number.ok().map(|x| {
            if x > 0 {
                telnet.write(&buf).unwrap();
                buf = [0; 256];
            }
        });
        thread::sleep(Duration::new(0, 5));
    }
}

impl MultiPlay {
    fn start(mut self) {
        // create a telnet thread for each
        let first = self.players.remove(0);
        if self.players.len() > 0 {
            for p in self.players.into_iter() {
                println!("spawning players in loop");
                thread::spawn(move || {
                    create_telnet_instance(p, false);
                });
            }
            // keep the actual main thread running
        }
        create_telnet_instance(first, true);
        // only need one handle to wait right?
    }
}

trait Event {
    fn respond(&mut self, input: Option<&String>, world: &mut WorldRep) -> Option<String>;
}

struct Chop;
impl Event for Chop {
    fn respond(&mut self, input: Option<&String>, world: &mut WorldRep) -> Option<String> {
        match input {
            Some(s) => {
                //println!("checking chop");
                if s.contains("commandchop") {
                    //println!("found chop");
                    return Some("chop".into());
                }
            }
            _ => {}
        }
        None
    }
}

// send enter response every now and then
struct HeartBeat {
    timer: SystemTime,
    on: bool,
}

impl Event for HeartBeat {
    fn respond(&mut self, input: Option<&String>, world: &mut WorldRep) -> Option<String> {
        match input {
            Some(s) => {
                if s.contains("hbeat-on") {
                    self.on = true;
                }
                if s.contains("hbeat-off") {
                    self.on = false;
                }
            }
            _ => {}
        }
        if self.on {
            if self.timer.elapsed().unwrap() > Duration::new(3, 0) {
                self.timer = SystemTime::now();
                return Some("l\n".to_string());
            }
        }
        return None;
    }
}

enum RestState {
    Resting,
    Standing,
}

struct AutoKill {
    on: bool,
    fighting: bool,
    targets: Vec<String>,
    command_i: usize,
    timer: SystemTime,
    state: RestState,
    target: String,
    kill_commands: Vec<String>,
}

impl Event for AutoKill {
    fn respond(&mut self, input: Option<&String>, world: &mut WorldRep) -> Option<String> {
        match input {
            // check if we are turning on or off the random walk
            Some(s) => {
                if s.contains("killcommand") {
                    //println!("entering kill command");
                    self.on = !self.on; // toggle the on nature of walk
                                        //println!("on is {:?}", self.on);
                    match s.split("/").nth(1) {
                        Some(t) => self.kill_commands.push(t.to_string().clone()),
                        _ => println!("no second argument given to autokill"),
                    }
                }
                if s.contains("autokill") {
                    //println!("changing AutoKill");
                    let prey = inbetween("autokill-", "-", s);
                    //println!("setting on kill for -{}-", prey);
                    self.targets.push(prey.clone());
                    //println!("self.targets {:?}",self.targets);
                    log(format!("{:?}", self.targets));
                }
                // assume this is a room description
                if s.contains("not here")
                    || s.contains("cancel")
                    || s.contains("anyone")
                    || s.contains("confusion")
                    || s.contains("NOBITS")
                {
                    self.fighting = false;
                    world.pstate = PlayerState::Standing;
                    return None;
                }
                if s.contains("R.I.P") {
                    self.fighting = false;
                    world.pstate = PlayerState::Standing;
                    return Some("take all corpse".to_string());
                }
                if s.contains("kill") {
                    self.fighting = true;
                    world.pstate = PlayerState::Fighting;
                }
                // responses
                if self.timer.elapsed().unwrap() > Duration::new(4, 0) {
                    self.timer = SystemTime::now();
                    log(format!("checking fighting {}", self.fighting));
                    if !self.fighting {
                        // if no exits don't search for targets
                        match s.find("Exits") {
                            Some(ind) => {
                                let tail = s[ind..].to_string();
                                log(format!("tail{}tail", tail));
                                for t in self.targets.iter() {
                                    //println!("scanning for target --{}-- in --{}--",t,s);
                                    //take part of text after exits

                                    if tail.to_lowercase().contains(&t.to_lowercase()) {
                                        self.fighting = true;
                                        self.target = t.to_string();
                                        world.pstate = PlayerState::Fighting;
                                        log(format!("targeting{} ", t));
                                        return Some(format!("kill {}", t));
                                    }
                                }
                            }
                            _ => {}
                        }
                    }
                    if self.fighting {
                        //println!("tring kick");
                        if self.kill_commands.len() > 0 {
                            self.command_i = (self.command_i + 1) % self.kill_commands.len();
                            log(format!("triggerinreturning kick?{}", self.command_i));
                            return Some(self.kill_commands[self.command_i].clone());
                        }
                    }
                }
            }
            _ => {}
        }
        // if wew reach here return None
        None
    }
}

enum HungerResponse {
    Eating,
    Drinking,
}

struct Puppet;
impl Event for Puppet {
    fn respond(&mut self, input: Option<&String>, world: &mut WorldRep) -> Option<String> {
        match input {
            Some(s) => {
                if s.contains("ppt-") {
                    // this is a command to be echoed by the bot player
                    let command = inbetween("ppt-", "-", &s);
                    return Some(command);
                }
            }
            _ => {}
        }
        return None;
    }
}

struct Repeat;

impl Event for Repeat {
    fn respond(&mut self, input: Option<&String>, world: &mut WorldRep) -> Option<String> {
        match input {
            Some(s) => {
                if s.contains("rep-") {
                    let command = inbetween("rep-", "x", &s);
                    let times = inbetween("x", "-", &s);
                    println!(
                        "repeating {}",
                        command.repeat(times.parse::<usize>().unwrap()).to_string()
                    );
                    return Some(
                        format!("{}\n", command)
                            .repeat(times.parse::<usize>().unwrap())
                            .to_string(),
                    );
                }
            }
            _ => {}
        }
        None
    }
}

struct Grouping {}

impl Event for Grouping {
    fn respond(&mut self, input: Option<&String>, world: &mut WorldRep) -> Option<String> {
        match input {
            Some(s) => {
                if s.contains("says, 'follow") {
                    let speaker = s.split_whitespace().nth(0).unwrap();
                    return Some(format!("follow {}", speaker));
                }
                //if s.contains("group") {
                //   let speaker =  s.split_whitespace().nth(0).unwrap() ;
                //        return Some(format!("group {}",speaker))
                //}
            }
            _ => {}
        }
        return None;
    }
}

struct EatDrink {
    state: HungerResponse,
}

impl Event for EatDrink {
    fn respond(&mut self, input: Option<&String>, world: &mut WorldRep) -> Option<String> {
        match input {
            Some(s) => {
                if s.contains("hungry") || s.contains("STARVING") {
                    //self.state = HungerResponse::Eating;
                    //return Some("eat loaf".into());
                }
                if s.contains("thirst") {
                    //self.state = HungerResponse::Drinking;
                    //////println!("drinking bottle");
                    //return Some("drink bottle".into());
                }
                // second level
                if s.contains("You do not have that item.") | s.contains("Drink what?") {
                    match self.state {
                        HungerResponse::Eating => {
                            //println!("getting rabbit meat from bag");
                            return Some("get meat bag".into());
                        }
                        HungerResponse::Drinking => {
                            //println!("getting a dragonskin from bag");
                            return Some("get dragonskin bag".into());
                        }
                    }
                }
            }
            _ => {}
        }
        return None;
    }
}

struct CommandPosition {}

impl Event for CommandPosition {
    fn respond(&mut self, input: Option<&String>, world: &mut WorldRep) -> Option<String> {
        match input {
            Some(s) => {
                if s.contains("commandstand") {
                    return Some("stand".into());
                }
                if s.contains("commandrest") {
                    return Some("rest".into());
                }
            }
            _ => {}
        }
        return None;
    }
}

// similar is the maze walker which ensures that all points are hit in an area if possible

struct Mazer {
    exits: Option<Vec<String>>,
    pth: maze::Pather,
    on: bool,
    state: CommandState,
    timer: SystemTime,
}

impl Mazer {
    fn new() -> Mazer {
        let pather = Pather::setup(15, 15);
        Mazer {
            pth: pather,
            exits: None,
            on: false,
            state: CommandState::First,
            timer: SystemTime::now(),
        }
    }
}

impl Event for Mazer {
    fn respond(&mut self, input: Option<&String>, world: &mut WorldRep) -> Option<String> {
        match world.pstate {
            PlayerState::Fighting => {
                log(format!("can't maze"));
                return None;
            }
            _ => {}
        }
        match input {
            Some(s) => {
                if s.contains("Exits") {
                    // check for all the rooms you can go in
                    let exit_line = inbetween(":", "]", s);
                    log(format!("exit line is {}", exit_line));
                    let mut v = vec![];
                    //?? how should I check for each nswe without a bunch of ifs
                    "nesw"
                        .chars()
                        .map(|e| {
                            if !exit_line.contains(e) {
                                v.push(e.to_string())
                            }
                        })
                        .collect::<Vec<_>>();
                    let closed = exit_line.clone();
                    // this will get the dir enclosed in the parens
                    for c in closed.split("(") {
                        v.push(c.chars().nth(0).expect("no char").to_string());
                    }
                    self.exits = Some(v);
                    self.state = CommandState::Second;
                    log(format!("missing exits {:?}", self.exits));
                }
                if s.contains("mazer-on") {
                    self.on = true;
                }
                if s.contains("mazer-off") {
                    self.on = false;
                    // reset the path
                    *self = Mazer::new();
                }
            }
            _ => {}
        }
        match self.state {
            CommandState::Second => {
                if self.on {
                    if self.timer.elapsed().unwrap() > Duration::new(5, 0) {
                        // go to one of the existing possible exits
                        // change the world state to second
                        // so now call step knowing what we know about the possible exits
                        self.timer = SystemTime::now();
                        // go back to first
                        self.state = CommandState::First;
                        return self.exits.clone().map(|ex| return self.pth.step(ex));
                    }
                }
            }
            _ => {}
        }
        None
    }
}

struct RandomWalk {
    on: bool,
    return_path: Vec<String>,
    timer: SystemTime,
}

impl RandomWalk {
    fn add_in(&mut self, dir: String) {
        if self.return_path.len() == 0 {
            self.return_path.push(dir)
        } else if dir == "E" && self.return_path.last().map(|e| e != "W").unwrap() {
            self.return_path.push(dir);
        } else if dir == "W" && self.return_path.last().map(|e| e != "E").unwrap() {
            self.return_path.push(dir);
        } else if dir == "N" && self.return_path.last().map(|e| e != "S").unwrap() {
            self.return_path.push(dir);
        } else if dir == "S" && self.return_path.last().map(|e| e != "N").unwrap() {
            self.return_path.push(dir);
        }
    }
    fn go_back(&mut self) -> Option<String> {
        self.return_path.pop()
    }
}

impl Event for RandomWalk {
    fn respond(&mut self, input: Option<&String>, world: &mut WorldRep) -> Option<String> {
        match input {
            // check if we are turning on or off the random walk
            Some(s) => {
                if s.contains("makeroute") {
                    let order = inbetween("makeroute-", "-", &s);
                    self.return_path = order
                        .chars()
                        .map(|e| e.to_string())
                        .collect::<Vec<String>>();
                    self.return_path.reverse();
                }
                if s.contains("go-back") {
                    return self.go_back();
                }
                if s.contains("randw-on") {
                    //println!("changing random walking");
                    self.on = true; // toggle the on nature of walk
                                    //println!("on is {:?}", self.on);
                }
                if s.contains("randw-off") {
                    //println!("turning off randw");
                    self.on = false;
                }
                if self.timer.elapsed().unwrap() > Duration::new(5, 0) {
                    self.timer = SystemTime::now();
                    if self.on {
                        let ind = random::<usize>() % 4;
                        let dir = "NESW".chars().nth(ind).unwrap().to_string();
                        self.add_in(dir.clone());

                        return Some(dir);
                    }
                    if self.return_path.len() > 0 {
                        return self.go_back();
                    }
                }
            }
            _ => {}
        }
        // if wew reach here return None
        None
    }
}

struct Listeners {
    v: Vec<Box<dyn Event>>,
}

impl Listeners {
    fn new() -> Listeners {
        Listeners { v: vec![] }
    }
    fn setup_listeners() -> Listeners {
        let mut l = Listeners::new();
        // add the functions that we think we should respond to here
        let rand_walker = RandomWalk {
            on: false,
            return_path: vec![],
            timer: SystemTime::now(),
        };
        let mazer = Mazer::new();
        let auto_kill = AutoKill {
            on: false,
            command_i: 0,
            state: RestState::Standing,
            timer: SystemTime::now(),
            kill_commands: vec![],
            target: String::new(),
            fighting: false,
            targets: vec![],
        };
        let heart_beat = HeartBeat {
            timer: SystemTime::now(),
            on: false,
        };
        let eat_drink = EatDrink {
            state: HungerResponse::Eating,
        };
        let grp = Grouping {};
        let command_p = CommandPosition {};
        let chop = Chop;
        let puppet = Puppet;
        let rep = Repeat;
        l.add(Box::new(rand_walker));
        l.add(Box::new(rep));
        l.add(Box::new(puppet));
        l.add(Box::new(mazer));
        l.add(Box::new(chop));
        l.add(Box::new(command_p));
        l.add(Box::new(grp));
        l.add(Box::new(eat_drink));
        l.add(Box::new(auto_kill));
        l.add(Box::new(heart_beat));
        //return listeners
        l
    }
    fn add(&mut self, l: Box<dyn Event>) {
        self.v.push(l);
    }
    fn iterate_text(&mut self, history: &Vec<String>, world_rep: &mut WorldRep) -> Option<String> {
        let hist = history.last();
        self.update(history.last(), world_rep)
    }
    fn update(&mut self, input: Option<&String>, world_rep: &mut WorldRep) -> Option<String> {
        // create a world rep
        // how would we not use mut here, or maybe a mut outer and each event gets a ref
        let mut response = String::new();
        for e in self.v.iter_mut() {
            match e.respond(input, world_rep) {
                // otherwise is consumed each time
                Some(s) => {
                    response += &format!("{}\n", s);
                }
                _ => {}
            }
        }
        if response.len() > 0 {
            return Some(response);
        }
        // could change to return input to preevnt msg cloning need
        return None;
    }
}

fn main() {
    let monsters = vec![
        "ant", "bunny", "farmer", "cat", "mouse", "turkey", "boy", "calf", "goose", "crawl",
        "newbie", "rat", "chick", "crow", "cow", "rooster", "chicken", "bull", "turtle", "pig",
        "peccary", "squirrel",
    ];
    let rompus = Player::new("rompus", "---", vec!["kick"], monsters.clone());
    //let ogo = Player::new(
    //    "ogo",
    //    "00000",
    //    12346,
    //    vec![],
    //    monsters.clone()
    //);
    //let bok = Player::new(
    //    "bok",
    //    "00000",
    //    12347,
    //    vec!["kick", "strike"],
    //    monsters.clone()
    //);
    //let pthree = Player::new(
    //    "pthree",
    //    "-----",
    //    vec!["kick", "strike"],
    //    monsters.clone()
    //);
    //let eugeo = Player::new(
    //    "eugeo",
    //    "00000",
    //    vec!["kick", "strike"],
    //    monsters.clone()
    //);
    let multi = MultiPlay {
        players: vec![rompus],
    };
    multi.start();
    //listen("127.0.0.1:8080", |out| Server {
    //    out: out,
    //    v_senders: vec![],
    //})
    //.unwrap();
}
